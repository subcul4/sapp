//package main
package config

import (
	"fmt"
	"strings"
	"strconv"
	//"net"
	"os"
	"bufio"
)

type Config struct {
	connection_timeout int
	ack_timeout int
	ipaddr string
	port string
	listen_num int
	key_path string
	crt_path string
	allow_addresses map[string]int
	table_size int
	table_timeout int
}

const DEFAULT_CONN_TIMEOUT = 60
const DEFAULT_ACK_TIMEOUT = 60
const DEFAULT_IPADDR = "127.0.0.1"
const DEFAULT_PORT = "10000"
const DEFAULT_LISTEN_NUM = 3
const DEFAULT_KEY_PATH = ".ssh/server.key"
const DEFAULT_CRT_PATH = ".ssh/server.crt"
const DEFAULT_TABLE_SIZE = 5
const DEFAULT_TABLE_TIMEOUT = 60

func NewConfig() *Config{
	cfg := new(Config)
	cfg.connection_timeout = DEFAULT_CONN_TIMEOUT
	cfg.ack_timeout = DEFAULT_ACK_TIMEOUT
	cfg.ipaddr = DEFAULT_IPADDR
	cfg.port = DEFAULT_PORT
	cfg.listen_num = DEFAULT_LISTEN_NUM
	cfg.key_path = DEFAULT_KEY_PATH
	cfg.crt_path = DEFAULT_CRT_PATH
	cfg.allow_addresses = make(map[string]int)
	cfg.table_size = DEFAULT_TABLE_SIZE
	cfg.table_timeout = DEFAULT_TABLE_TIMEOUT
	return cfg
}

func (config *Config) GetListenInfo() (string, string, int, string, string){
	return config.ipaddr, config.port, config.listen_num, config.crt_path, config.key_path
}

func (config *Config) GetTableInfo() (int, int){
	return config.table_size, config.table_timeout
}

func (config *Config) GetConnInfo() (int, int){
	return config.connection_timeout, config.ack_timeout
}

func (config *Config) GetAllowIPInfo() map[string]int {
	return config.allow_addresses
}

func (config *Config) ParseConfig(file_name string){

	fp, err := os.Open(file_name)
	if err != nil {
		fmt.Print("cannot open .conf file")
	}
	defer fp.Close()

	scanner := bufio.NewScanner(fp)

	for scanner.Scan() {
		if scanner.Text() == ""{
			continue
		}
		var line string
		var item string
		var content string

		line = strings.Replace(scanner.Text(), " ", "", -1)
		fmt.Sscanf(line,"%s\t%s",&item,&content)
		content = strings.Replace(content, "\"", "", -1)

		if item == "ConnectionTimeout"{
			tmp, err := strconv.Atoi(content)
			if err == nil {
				config.connection_timeout = tmp
			}
		}else if item == "AckTimeout" {
			tmp, err := strconv.Atoi(content)
			if err == nil {
				config.ack_timeout = tmp
			}
		}else if item == "IPAdress" {
			config.ipaddr = content
		}else if item == "Port" {
			config.port = ":" + content
		}else if item == "ListenLimit"{
			tmp, err := strconv.Atoi(content)
			if err == nil {
				config.listen_num = tmp
			}
		}else if item == "KeyPath"{
			config.key_path = content
		}else if item == "CrtPath"{
			config.crt_path = content
		}else if item == "AllowAddresses"{
			split_content := strings.Split(content,",")
			for _, s := range split_content{
				if !validIPaddr(s){
					continue
				}
				config.allow_addresses[s] = 1
			}
		}else if item == "TableSize"{
			tmp, err := strconv.Atoi(content)
			if err == nil {
				config.table_size = tmp
			}
		}else if item == "TableTimeout"{
			tmp, err := strconv.Atoi(content)
			if err == nil {
				config.table_timeout = tmp
			}
		}
	}
}

func (config *Config) ShowConfig() {
	fmt.Println(config.connection_timeout)
	fmt.Println(config.ack_timeout)
	fmt.Println(config.ipaddr)
	fmt.Println(config.port)
	fmt.Println(config.listen_num)
	fmt.Println(config.key_path)
	fmt.Println(config.crt_path)
	fmt.Println(config.allow_addresses)
	for _, d := range config.allow_addresses{
		fmt.Println(d)
	}
	fmt.Println(len(config.allow_addresses))
	fmt.Println(config.table_size)
	fmt.Println(config.table_timeout)
}

func validIPaddr(ipaddr string) bool {
	var ip1, ip2, ip3, ip4 uint8
	tmp := strings.Split(ipaddr,"/")[0]
	_, err := fmt.Sscanf(tmp,"%d.%d.%d.%d",&ip1,&ip2,&ip3,&ip4)
	if err != nil{
		fmt.Printf("IPaddr err%#v\n",err)
		return false
	}
	return true
}
/*
func main(){
	config := NewConfig()
	config.ParseConfig("../sapp.conf")
	return
}
*/
