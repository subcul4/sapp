package main

import (
	"fmt"
	"net"
	"os"
	"errors"
	"golang.org/x/net/netutil"
	"crypto/tls"
	"strings"
	"time"
	"io"
	"./table"
	"./config"
)

type Status int
const (
	NORMAL Status = iota
	MESSAGE
)

type ConnInfo struct{
	conn net.Conn
	status Status
	conn_timeout int
	ack_timeout int
}

const (
	OK_CODE = iota
	ER_CODE1
	ER_CODE2
	ER_CODE3
	ER_CODE4
)

var DENY_TABLE = table.NewIPTable()

func main() {
	var cfg = config.NewConfig()
	cfg.ParseConfig("./sapp.conf")

	table_size, table_timeout := cfg.GetTableInfo()
	conn_timeout, ack_timeout := cfg.GetConnInfo()
	ipaddr, port, listen_num, crt_path, key_path := cfg.GetListenInfo()

	DENY_TABLE.SetConfig(table_size, table_timeout)

	listener, err:= setup_listen(ipaddr, port, listen_num, crt_path, key_path)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	fmt.Printf("Listening...\n")

	defer listener.Close()

	for {
		conn, err := listener.Accept()
		if err != nil {
			fmt.Printf("Accept error: %s\n",err)
			continue
		}

		ip := paser_IP_from_conn(conn)
		fmt.Printf("Connect from %s\n",ip)
		if DENY_TABLE.ExistCheck(ip){
			sendCommands(conn,"DENY")
			conn.Close()
			continue
		}else if !is_allow_address(ip,cfg.GetAllowIPInfo()){
			DENY_TABLE.AddTable(ip,time.Now())
			sendCommands(conn,"DENY")
			conn.Close()
			continue
		}
		connInfo := ConnInfo{conn, NORMAL, conn_timeout, ack_timeout}
		go server_process(&connInfo)
	}
}

func setup_listen(ipaddr string, port string, listen_num int, crt_path string, key_path string) (net.Listener, error) {
	cer, err := tls.LoadX509KeyPair(crt_path,key_path)
	if err != nil {
		return nil, errors.New(fmt.Sprintf("TLS Pair Error:%#v",err))
	}

	conf := &tls.Config{Certificates: []tls.Certificate{cer}}
	l_, err := tls.Listen("tcp",ipaddr + port,conf)
	if err != nil {
		return nil, errors.New(fmt.Sprintf("Listen Error:%#v",err))
	}
	l := netutil.LimitListener(l_, listen_num)

	return l,nil
}

func server_process(connInfo *ConnInfo) {
	defer connInfo.conn.Close()
	connInfo.conn.SetReadDeadline(time.Now().Add(time.Duration(connInfo.conn_timeout) * time.Minute))
	buf := make([]byte,1024)
	for {
		fmt.Printf("Command Waiting...\n")
		n, err := connInfo.conn.Read(buf)
		if err != nil {
			operr, ok := err.(*net.OpError)
			if ok && operr.Timeout(){
				fmt.Printf("Timeout\n")
				return
			} else if err == io.EOF {
				fmt.Printf("Socket Close\n")
				return
			}
		}

		res := parse_cmd(connInfo, strings.ToUpper(string(buf[:n])))
		if res == -1 {
			fmt.Printf("Terminate connection\n")
			connInfo.conn.Close()
			return
		}
	}
}

//parse the received command, call a corresponded function and send the response.
func parse_cmd(connInfo *ConnInfo, str string) int {
	str = strings.Replace(str,"\r\n","",-1)
	str = strings.Replace(str,"\n","",-1)
	str_vec := strings.Split(str," ")
	n := len(str_vec)

	fmt.Printf("[RECV] <== %s\n",str)

	cmd := str_vec[0]

	if cmd == "REQUEST"{
		if n < 2 {
			response := fmt.Sprintf("-ERR %d",ER_CODE3)
			sendCommands(connInfo.conn, response)
			return 0
		}

		mtype := str_vec[1]

		situation := makeSituation(str_vec[2:],400)
		res := request_func(connInfo, mtype, situation)
		return res

	} else if cmd == "MESSAGE" {
		if n < 2 {
			response := fmt.Sprintf("-ERR %d",ER_CODE3)
			sendCommands(connInfo.conn, response)
			return 0
		}
		situation := makeSituation(str_vec[1:],800)
		res := message_func(connInfo,situation)
		return res

	} else if cmd == "TERMINATE"{
		res := terminate_func(connInfo)
		return res

	} else {
		response := fmt.Sprintf("-ERR %d",ER_CODE1)
		sendCommands(connInfo.conn, response)
	}
	return 0
}


//if the server received the ACK commands, "true" is returned.
func recvAck(connInfo *ConnInfo) bool {
	connInfo.conn.SetReadDeadline(time.Now().Add(time.Duration(connInfo.ack_timeout) * time.Second))
	var command string
	buf := make([]byte,1024)
	n, err := connInfo.conn.Read(buf)
	if err != nil{
		fmt.Printf("%s\n",err)
		return false
	}

	recvBuf := strings.ToUpper(string(buf[:n]))
	fmt.Sscanf(recvBuf,"%s",&command)
	if command == "ACK" {
		return true
	}
	return false
}

func sendCommands(conn net.Conn,command string){
	fmt.Printf("[SEND] ==> %s\n",command)
	conn.Write([]byte(command))
}

func makeSituation(situation_vec []string, max_num int) string{
	var res string
	for _, s := range situation_vec{
		res += s + " "
	}
	tmp := []rune(res)
	if len(tmp) > max_num {
		res = string(tmp[:max_num])
	}
	return res
}

func request_func(connInfo *ConnInfo, mtype string, situation string) int {
	resp_situation := situation
	fmt.Printf(mtype)

	if mtype != "DECLARE" && mtype != "PRAY" && mtype != "FORCE" {
		r := fmt.Sprintf("-ERR %d",ER_CODE1)
		sendCommands(connInfo.conn, r)
		return 0
	}

	if mtype == "PRAY" && strings.Contains(situation, "手を繋いだ") {
		resp_situation = "私は躊躇いながら手を握り返した"

		response := fmt.Sprintf("+OK %d ACCEPT %s",OK_CODE, resp_situation)
		sendCommands(connInfo.conn, response)
		if recvAck(connInfo) {
			sendCommands(connInfo.conn, "ACK")
			connInfo.status = MESSAGE
		}else {
			r := fmt.Sprintf("-ERR %d",ER_CODE4)
			sendCommands(connInfo.conn, r)
		}
		return 0
	}

	if strings.Contains(situation, "キスをした"){
		ip := paser_IP_from_conn(connInfo.conn)
		DENY_TABLE.AddTable(ip,time.Now())
		DENY_TABLE.ShowTable()
		resp_situation = "「ごめん、そういうのじゃないから」私はぐいと押し返した。"
		response := fmt.Sprintf("+OK %d DECLINE %s",OK_CODE, resp_situation)
		sendCommands(connInfo.conn, response)
		if recvAck(connInfo){
			sendCommands(connInfo.conn, "ACK")
			return -1
		}else {
			r := fmt.Sprintf("-ERR %d",ER_CODE4)
			sendCommands(connInfo.conn, r)
		}

	}
	resp_situation = "「じゃあ、また今度ね」と私はそそくさとその場を後にした"

	response := fmt.Sprintf("+OK %d SUSTAIN %s",OK_CODE, resp_situation)
	sendCommands(connInfo.conn, response)
	if recvAck(connInfo) {
		sendCommands(connInfo.conn, "ACK")
		return -1
	}else {
		r := fmt.Sprintf("-ERR %d",ER_CODE4)
		sendCommands(connInfo.conn, r)
	}


	/*
	if mtype == "DECLARE"{

		response := fmt.Sprintf("+OK %d ACCEPT %s",OK_CODE, resp_situation)
		sendCommands(connInfo.conn, response)
		if recvAck(connInfo) {
			sendCommands(connInfo.conn, "ACK")
			connInfo.status = MESSAGE
		}else {
			r := fmt.Sprintf("-ERR %d",ER_CODE4)
			sendCommands(connInfo.conn, r)
		}

	}else if mtype == "PRAY"{

		response := fmt.Sprintf("+OK %d SUSTAIN %s",OK_CODE, resp_situation)
		sendCommands(connInfo.conn, response)
		if recvAck(connInfo) {
			sendCommands(connInfo.conn, "ACK")
			return -1
		}else {
			r := fmt.Sprintf("-ERR %d",ER_CODE4)
			sendCommands(connInfo.conn, r)
		}

	} else if mtype == "FORCE" {
		
		ip := paser_IP_from_conn(connInfo.conn)
		DENY_TABLE.AddTable(ip,time.Now())
		DENY_TABLE.ShowTable()
		response := fmt.Sprintf("+OK %d DECLINE %s",OK_CODE, resp_situation)
		sendCommands(connInfo.conn, response)
		if recvAck(connInfo){
			sendCommands(connInfo.conn, "ACK")
			return -1
		}else {
			r := fmt.Sprintf("-ERR %d",ER_CODE4)
			sendCommands(connInfo.conn, r)
		}

	} else {

		r := fmt.Sprintf("-ERR %d",ER_CODE1)
		sendCommands(connInfo.conn, r)
	}
	*/
	return 0
}

func message_func(connInfo *ConnInfo, message string) int {
	var response string
	resp_message := message

	if strings.Contains(message,"もう少しこのままでも"){
		resp_message = "「いいよ」なんでもないように答えたつもりで、うまく声が出なかった。"
	}else{
		resp_message = "..."
	}
	if connInfo.status == MESSAGE {
		response = fmt.Sprintf("ACK")
	} else {
		response = fmt.Sprintf("-ERR %d",ER_CODE4)
	}
	sendCommands(connInfo.conn, response + " " + resp_message)
	return 0
}

func terminate_func(connInfo *ConnInfo) int {
	response := fmt.Sprintf("ACK")
	sendCommands(connInfo.conn, response)
	return -1
}

func paser_IP_from_conn(conn net.Conn) string {
	network_addr := conn.RemoteAddr().String()
	slice := strings.Split(network_addr,":")
	return slice[0]
}

func is_allow_address(ipaddr string, allow_addresses map[string]int) bool {
	for k, _ := range allow_addresses{
		if ipaddr == k {
			return true
		}

		if strings.Contains(ipaddr,"/") {
			_, ipnet, err := net.ParseCIDR(k)
			if err != nil {
				fmt.Println(err)
			}
			check_ip := net.ParseIP(ipaddr)
			if ipnet.Contains(check_ip){
				return true
			}
		}
	}
	return false
}
