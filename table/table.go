package table

import (
	"fmt"
	"sync"
	"time"
)



type IP_TABLE struct{
	ipaddrs map[string]time.Time
	table_mutex sync.RWMutex
	max_size int
	timeout int
}

func NewIPTable() *IP_TABLE {
	ipaddrs_ := map[string]time.Time{}
	table_mutex_ := sync.RWMutex{}

	return &IP_TABLE{ipaddrs: ipaddrs_, table_mutex:table_mutex_, max_size:5, timeout:10}
}

func (ip_table *IP_TABLE) SetConfig(arg_max_size int, arg_timeout int) {
	ip_table.max_size = arg_max_size
	ip_table.timeout = arg_timeout
}

func (ip_table *IP_TABLE) ShowTable() {
	ip_table.table_mutex.RLock()
	defer ip_table.table_mutex.RUnlock()

	for k,t := range ip_table.ipaddrs {
		fmt.Printf("%s : %s\n",k,t)
	}
}

func (ip_table *IP_TABLE) ExistCheck(addr string) bool {
	ip_table.table_mutex.RLock()
	defer ip_table.table_mutex.RUnlock()

	_, ok := ip_table.ipaddrs[addr]
	now := time.Now()
	limit := ip_table.ipaddrs[addr].Add(time.Duration(ip_table.timeout) * time.Second)
	if now.After(limit) {
		return false
	}
	return ok
}

func (ip_table *IP_TABLE) AddTable(addr string, t time.Time) bool {
	ip_table.table_mutex.Lock()
	defer ip_table.table_mutex.Unlock()

	if len(ip_table.ipaddrs) >= ip_table.max_size {
		res := ip_table.CleanTable()
		if(!res){
			fmt.Printf("Cannot add addr = %s\n", addr)
			return false
		}
	}

	ip_table.ipaddrs[addr] = t
	fmt.Printf("Added addr = %s\n", addr)
	return true

}

func (ip_table *IP_TABLE) DelTable(addr string) {
	ip_table.table_mutex.Lock()
	defer ip_table.table_mutex.Unlock()

	if ip_table.ExistCheck(addr) {
		delete(ip_table.ipaddrs,addr)
	}
}

func (ip_table *IP_TABLE) CleanTable() bool {
	var cnt int
	now := time.Now()
	for k, t := range ip_table.ipaddrs {
		limit := t.Add(time.Duration(ip_table.timeout) * time.Second)
		fmt.Printf("Time: %s, Limit; %s\n",t,limit)
		fmt.Printf("%v\n",now.After(limit))
		if now.After(limit) {
			fmt.Printf("Exceed the limit\n")
			delete(ip_table.ipaddrs,k)
			cnt++
		}
	}
	if cnt > 0 {
		return true
	}
	return false
}
