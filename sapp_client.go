package main

import (
	"fmt"
	"net"
	"os"
	"bufio"
	"crypto/tls"
	"time"
)

func main() {
	conf := &tls.Config {
		InsecureSkipVerify: true,
	}

	//conn, err := tls.Dial("tcp","127.0.0.1:443",conf)
	conn, err := tls.Dial("tcp","127.0.0.1:10000",conf)

	//conn, err := net.Dial("tcp","127.0.0.1:10000")
	if err != nil {
		fmt.Printf("Connection error: %s\n",err)
		return
	}

	stdin := bufio.NewScanner(os.Stdin)
	for stdin.Scan(){
		message := stdin.Text()
		if message != "exit" {
			sendMsg := fmt.Sprintf("%s\n",message)
			conn.Write([]byte(sendMsg))
		} else {
			conn.Close()
			break
		}
		buf := make([]byte,1024)

		conn.SetReadDeadline(time.Now().Add(time.Duration(3 * time.Minute)))

		n, err := conn.Read(buf)
		if n == 0 {
			break
		}
		if err != nil {
			fmt.Println(err)
			break
		}
		fmt.Printf("[RECV] ==> %s\n",string(buf[:n]))
		parse_response(conn,string(buf[:n]))
	}
}
func parse_response(conn net.Conn,str string){
	var code string
	var code_num int
	var cmd string
	var mtype string
	fmt.Sscanf(str,"%s %d %s %s",&code, &code_num, &cmd, &mtype)
	//fmt.Printf("%s\n",mtype)
/*
	if code != "+OK" {
		return
	}

	if cmd == "REQUEST" {
		reaction(conn, mtype)
	}

	if cmd == "TERMINATE" {
		sendCommands(conn, "ACK")
	}

	if cmd == "MESSAGE" {
		sendCommands(conn, "ACK")
	}
*/
}

func reaction(conn net.Conn, mtype string) {
	if mtype == "ACCEPT" || mtype == "DECLINE" || mtype == "SUSTAIN" {
		sendCommands(conn, "ACK")
		return
	}
	sendCommands(conn,"MTYPE ERROR")
}

func ack(conn net.Conn) bool {
        buf := make([]byte,1024)
        n, err := conn.Read(buf)
        if err != nil{
                return false
        }
        command := string(buf[:n])
        if(command == "ACK"){
                return true
        }
        return false
}

func sendCommands(conn net.Conn, command string) {
	fmt.Printf("[SEND] ==> %s\n",command)
	conn.Write([]byte(command))
}

